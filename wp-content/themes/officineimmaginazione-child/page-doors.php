<?php
/*
Template Name: Pagina Porte
*/
?>

<main class="doors <?php echo get_post_meta($post->ID, 'pd-product-color', true)?>">
    <div class="d-flex align-items-center full-img">
        <div class="d-flex align-items-center container">
            <div class="col-8 col-sm-6 col-md-5 banner-text">
                <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-banner-text', true)); ?>
            </div>
            <div class="col-12 col-md-7 banner-img">
                <?php get_tag_picture(get_post_meta($post->ID, 'pd-banner-image', true)['id']); ?>
            </div>
        </div>
        <div class="d-flex justify-content-center align-items-center scroll">
            <div class="scroll-item">
                <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/scroll.gif" alt="scroll"/>
                <?php _e("Scroll","officineimmaginazione"); ?>
            </div>
        </div>
    </div>
    
    <?php if(get_post_meta($post->ID, 'pd-first-box-title', true) != ""){ ?>
        <div id="scroll-anchor" class="d-flex container first-content">
            <div class="col-12 offset-sm-1 col-sm-3 offset-md-1 col-md-4 offset-lg-2 col-lg-3 box-left">
                <?php echo get_post_meta($post->ID, 'pd-first-box-title', true);?>
            </div>
            <div class="col-12 col-sm-7 col-md-6 col-xl-5 box-right">
                <?php echo get_post_meta($post->ID, 'pd-first-box-content', true);?>
            </div>
        </div>
    <?php } ?>
    
    <?php if(get_post_meta($post->ID, 'pd-second-box-title', true) != ""){ ?>
        <div class="d-flex align-items-center container second-content">
            <div class="d-flex justify-content-center col-12  offset-sm-1 col-sm-4 col-md-5  offset-lg-1 col-lg-3 boxed-content">
                <?php echo get_post_meta($post->ID, 'pd-second-box-title', true);?>
            </div>
            <div class="d-flex justify-content-center col-12 offset-sm-1 col-sm-6 col-md-5 offset-lg-4 col-lg-3 offset-xl-5 boxed-btn">
                <a href="<?php echo get_post_meta($post->ID, 'pd-second-box-button_url', true);?>" class="btn"><?php echo get_post_meta($post->ID, 'pd-second-box-button_text', true);?></a>
            </div>
            <div class="effect overflowed"></div>
        </div>
    <?php } ?>
    
    
    <?php 
        $position = array("first","second","third","fourth","fifth");
        $icons = array("performance.svg","design.svg","endurance.svg");
        $number = array("1","2","3");
    ?>
    <div class="d-flex container product-info">
        <div class="col-5 col-sm-6 col-md-5 image">
            <div class="d-flex flex-column align-items-center tablet-select">
                <?php for($i = 0; $i<3 ; $i++){ ?>
                    <?php if($i == 0){ ?>
                        <div id="<?php echo $number[$i]?>" class="d-flex align-items-center option show">
                            <?php echo get_post_meta($post->ID, 'pd-product-title-'.$number[$i], true); ?>
                            <img class="option-icon" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/<?php echo $icons[$i]?>" alt="icona"/>
                        </div>
                    <?php } else { ?>
                        <div id="<?php echo $number[$i]?>" class="d-flex align-items-center option">
                            <?php echo get_post_meta($post->ID, 'pd-product-title-'.$number[$i], true); ?>
                            <img class="option-icon" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/<?php echo $icons[$i]?>" alt="icona"/>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php get_tag_picture(get_post_meta($post->ID, 'pd-product-image', true)['id'])?>
        </div>
        <div class="col-2 info-select">
            <?php for($i = 0; $i<3 ; $i++){ ?>
                <?php if($i == 0){ ?>
                    <div id="<?php echo $number[$i]?>" class="d-flex align-items-center option show">
                        <?php echo get_post_meta($post->ID, 'pd-product-title-'.$number[$i], true); ?>
                        <img class="option-icon" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/<?php echo $icons[$i]?>" alt="icona"/>
                    </div>
                <?php } else { ?>
                    <div id="<?php echo $number[$i]?>" class="d-flex align-items-center option">
                        <?php echo get_post_meta($post->ID, 'pd-product-title-'.$number[$i], true); ?>
                        <img class="option-icon" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/<?php echo $icons[$i]?>" alt="icona"/>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="offset-1 col-4  offset-sm-0 col-sm-6 offset-md-1 col-md-6 offset-lg-1 col-lg-4 content-box">
            <?php for($i = 0; $i<3 ; $i++){ ?>
                <div id="c<?php echo $number[$i]?>" class="content">
                    <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-product-content-'.$position[$i], true)); ?>
                </div>
            <?php } ?>
        </div>
        <div class="box-border"></div>
    </div>
    
    
    <div class="d-flex container d-sm-none mobile-product-info">
        <div id="accordion">
            <div class="card">
                <?php for($i=0; $i<3; $i++){ ?>
                    <div class="offset-1 col-10 card-header" id="heading<?php echo $position[$i]; ?>">
                      <h5 class="mb-0">
                        <a class="btn" data-toggle="collapse" data-target="#collapse<?php echo $position[$i]; ?>" aria-expanded="true" aria-controls="collapse<?php echo $position[$i]; ?>">
                          <?php echo get_post_meta($post->ID, 'pd-product-title-'.$number[$i], true); ?>
                        </a>
                      </h5>
                    </div>
                    <div id="collapse<?php echo $position[$i]; ?>" class="offset-1 col-10 collapse" aria-labelledby="heading<?php echo $position[$i]; ?>" data-parent="#accordion">
                      <div class="card-body">
                          <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-product-content-'.$position[$i], true)); ?>
                      </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    
    <?php if(get_post_meta($post->ID, 'pd-solutions-content-first', true) != ""){ ?>
        <div class="container product-solution">
            <div class="col-12 offset-sm-2 col-sm-8 text-center">
                <h2><?php echo get_post_meta($post->ID, 'pd-solutions-title', true);?></h2>
            </div>
            <?php for($i = 0; $i < 5; $i++){
                if(get_post_meta($post->ID, 'pd-solutions-content-'.$position[$i], true) != ""){
                    if($i%2 == 0){ ?>
                        <div id="solution-<?php echo $i;?>" class="single-solution">
                            <div class="d-flex solution-box box-left">
                                <div class="d-flex flex-column justify-content-center col-12 offset-sm-0 col-sm-5 offset-md-1 col-md-5 offset-lg-2 col-lg-4 offset-xl-3 col-xl-3 solution-content">
                                    <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-solutions-content-'.$position[$i], true)); ?>
                                </div>
                                <div class="col-12 offset-sm-0 col-sm-7 col-md-6 col-lg-5 solution-image">
                                    <?php get_tag_picture(get_post_meta($post->ID, 'pd-solutions-image-'.$position[$i], true)['id']); ?>
                                </div>
                            </div>
                        </div>
                    <?php }
                    else{ ?>
                        <div id="solution-<?php echo $i;?>" class="single-solution">
                            <div class="d-flex solution-box box-right">
                                <div class="col-12  offset-sm-0 col-sm-7 offset-md-1 col-md-5 offset-lg-2 col-lg-4 offset-xl-1 col-xl-5 solution-image">
                                    <?php get_tag_picture(get_post_meta($post->ID, 'pd-solutions-image-'.$position[$i], true)['id']); ?>
                                </div>
                                <div class="d-flex flex-column justify-content-center col-12 offset-sm-0 col-sm-5 offset-md-1 col-md-5 offset-lg-1 col-lg-4 offset-xl-0 col-xl-3 solution-content">
                                    <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-solutions-content-'.$position[$i], true));?>
                                </div>
                            </div>
                        </div>
                    <?php }
                }
            } ?>
        </div>
    <?php } ?>
    
    <?php if(get_post_meta($post->ID, 'pd-applications-left', true) != "" || get_post_meta($post->ID, 'pd-applications-right', true) != ""){ ?>
        <div class="d-flex container product-application">
            <div class="overflowed">
            </div>
            <div class="row boxed-content">
                <div class="col-12 offset-lg-1 col-lg-3 title">
                    <?php echo get_post_meta($post->ID, 'pd-applications-title', true);?>
                </div>
                <div class="col-6 offset-sm-1 col-sm-5 offset-lg-1 col-lg-3 application-content">
                    <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-applications-left', true));?>
                </div>
                <div class="col-6 offset-sm-0 col-sm-5 offset-lg-1 col-lg-3 application-content">
                    <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-applications-right', true));?>
                </div>
            </div>
        </div>
    
    
        <div class="row ie-application">
            <div class="col-12 offset-lg-1 col-lg-2 title">
                <?php echo get_post_meta($post->ID, 'pd-applications-title', true);?>
            </div>
            <div class="col-6 offset-sm-1 col-sm-5 offset-lg-1 col-lg-3 application-content">
                <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-applications-left', true));?>
            </div>
            <div class="col-6 offset-sm-0 col-sm-5 offset-lg-1 col-lg-3 application-content">
                <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pd-applications-right', true));?>
            </div>
        </div>
    <?php } ?>
    
    <?php if(get_post_meta($post->ID, 'pd-projects-shortcode', true) != ""){ ?>
        <div class="other-projects">
            <h3><?php echo get_post_meta($post->ID, 'pd-projects-title', true);?></h3>
            <div class="projects-list">
                <div class="container">
                    <?php echo do_shortcode(get_post_meta($post->ID, 'pd-projects-shortcode', true))?>
                    <div class="d-flex justify-content-center align-items-center mobile-box">
                        <a class="mobile-discover"><?php _e("SCOPRI","officineimmaginazione"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</main>