<?php
/*
  Plugin Name: Officine Immaginazione Social Profile Widget
  Plugin URI: http://www.officineimmaginazione.com
  Description: 
  Author: Officine Immaginazione
  Author URI: https://www.officineimmaginazione.com
*/

class OI_Social_Profile extends WP_Widget {
	
	function __construct() {
		parent::__construct(
			'OI_Social_Profile',
			__('Profili Social', 'officineimmaginazione'),
			array ('description' => __('Link ai vari profili social dell\'utente', 'officineimmaginazione'))
		);
	}
	
	public function form($instance) {
        isset($instance['facebook']) ? $facebook = $instance['facebook'] : null;
        isset($instance['google']) ? $google = $instance['google'] : null;
		isset($instance['youtube']) ? $youtube = $instance['youtube'] : null;
        isset($instance['instagram']) ? $instagram = $instance['instagram'] : null;
        isset($instance['linkedin']) ? $linkedin = $instance['linkedin'] : null;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e('Facebook:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="text" value="<?php echo esc_attr($facebook); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('google'); ?>"><?php _e('Google:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('google'); ?>" name="<?php echo $this->get_field_name('google'); ?>" type="text" value="<?php echo esc_attr($google); ?>">
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('youtube'); ?>"><?php _e('Youtube:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" type="text" value="<?php echo esc_attr($youtube); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('instagram'); ?>"><?php _e('Instagram:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>" type="text" value="<?php echo esc_attr($instagram); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('linkedin'); ?>"><?php _e('Linkedin:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('linkedin'); ?>" name="<?php echo $this->get_field_name('linkedin'); ?>" type="text" value="<?php echo esc_attr($linkedin); ?>">
        </p>
        <?php
    }
	
	public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['facebook'] = (!empty($new_instance['facebook']) ) ? strip_tags($new_instance['facebook']) : '';
        $instance['google'] = (!empty($new_instance['google']) ) ? strip_tags($new_instance['google']) : '';
		$instance['youtube'] = (!empty($new_instance['youtube']) ) ? strip_tags($new_instance['youtube']) : '';
        $instance['instagram'] = (!empty($new_instance['instagram']) ) ? strip_tags($new_instance['instagram']) : '';
        $instance['linkedin'] = (!empty($new_instance['linkedin']) ) ? strip_tags($new_instance['linkedin']) : '';
        return $instance;
    }
	
	public function widget($args, $instance) {
		$facebook = $instance['facebook'];
		$google = $instance['google'];
		$youtube = $instance['youtube'];
        $instagram = $instance['instagram'];
        $linkedin = $instance['linkedin'];

        $facebook_profile = '<a href="'.$facebook.'" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a>';
        $google_profile = '<a href="'.$google.'" target="_blank"><i class="fab fa-google-plus-square fa-lg"></i></a>';
        $youtube_profile = '<a href="'.$youtube.'" target="_blank"><i class="fab fa-youtube-square fa-lg"></i></a>';
        $instagram_profile = '<a href="'.$instagram.'" target="_blank"><i class="fab fa-instagram fa-lg"></i></a>';
        $linkedin_profile = '<a href="'.$linkedin.'" target="_blank"><i class="fab fa-linkedin fa-lg"></i></a>';
        
		echo $args['before_widget'];
		echo '<div class="social-icons">';
		echo (!empty($facebook)) ? $facebook_profile : null;
        echo (!empty($instagram)) ? $instagram_profile : null;
        echo (!empty($linkedin)) ? $linkedin_profile : null;
		echo (!empty($google)) ? $google_profile : null;
		echo (!empty($youtube)) ? $youtube_profile : null;
		echo '</div>';
		echo $args['after_widget'];
	}
}