<?php
/*
Theme Name: Officine Immaginazione
Theme URI: http://www.officineimmaginazione.com/wordpress-themes
Description: OfficineImmaginazione is a Wordpress theme based on Bootstrap, FontAwesome, ecc.
Version: 1.0.0
Author: Officine Immaginazione
Author URI: http://www.officineimmaginazione.com
License: All right is reserved

Theme activation
*/
function oi_breadcrumb($postId, $tax) {
	if(!is_home()) {
		$topcat = "";
		if (is_single()) {
			$cat = get_the_terms($postId, $tax);
			// Find the last child category
			foreach ($cat as $last_category){
				foreach($cat as $i => $cur_cat)
					if($cur_cat->parent == $last_category->term_id)
						$last_category = $cur_cat;
			}
			$cat = $last_category;
			$cat_parents = get_category_parents($cat, true, ' > ');
			$i = 0;
		}
		echo '<ol class="breadcrumb">';
		if (is_single()) {
			foreach(explode(" > ", $cat_parents) as $single_parents){
				if (($single_parents != "")) {
					echo '<li>';
					echo  $single_parents;
					echo '</li>';
				}
				else {
					$i++;
				}
			}
			echo '<li>';
				echo '<span class="text-uppercase">' . get_the_title() . '</span> ' . get_post_custom()["prdo-subtitle"][0];
			echo '</li>';
		}
		elseif (is_category()) {
			$cat = get_query_var('cat');
			$cat_parents = get_category_parents($cat, true, ' > ' );
			foreach(explode(" > ", $cat_parents) as $single_parents){
				if (($single_parents != "")){
					echo '<li>';
					echo  $single_parents ;
					echo '</li>';
				}
				else{
					$i++;
				}
			}
		}
		elseif (is_tax("product-category") || is_tax("product-tag")) {
			$cat = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'))->term_id;
			$cat_parents = get_category_parents($cat, true, ' > ' );
			foreach(explode(" > ", $cat_parents) as $single_parents){
				if ($single_parents != ""){
					echo '<li>';
					echo  $single_parents ;
					echo '</li>';
				}
				else{
					$i++;
				}
			}
		}
		elseif (is_page() && (!is_front_page())) {
				//print_r(get_post_ancestors());
				foreach (get_post_ancestors() as $ancestor) {
					echo '<li>';
					echo get_the_title($ancestor) . " >&nbsp;";
					echo '</li>';
				}
				echo '<li class="active">';
				the_title();
				echo '</li>';
		} elseif (is_tag()) {
				echo '<li>Tag: ';
				single_tag_title();
				echo '</li>';
		} elseif (is_day()) {
				echo'<li>Archive for ';
				the_time('F jS, Y');
				echo'</li>';
		} elseif (is_month()) {
				echo'<li>Archive for ';
				the_time('F, Y');
				echo'</li>';
		} elseif (is_year()) {
				echo'<li>Archive for ';
				the_time('Y');
				echo'</li>';
		} elseif (is_author()) {
				echo'<li>Author Archives';
				echo'</li>';
		} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
				echo '<li>Blog Archives';
				echo'</li>';
		} elseif (is_search()) {
				echo'<li>Search Results';
				echo'</li>';
		}
		echo '</ol></h4>';
	}
}

/*
function oi_breadcrumbs() {
  
  $delimiter = '/';
  $home = 'Home'; 
  $before = '<span class="current">';
  $after = '</span>';
  
  if ( !is_home() && !is_front_page() || is_paged() ) {
  
    echo '<div id="breadcrumbs">';
  
    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
    
  
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . single_cat_title('', false) . $after;
  
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
  
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
  
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
  
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }
  
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after . $delimiter;
  
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
  
    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;
  
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;
  
    } elseif ( is_search() ) {
      echo $before . get_search_query() . $after;
  
    } elseif ( is_tag() ) {
      echo $before . single_tag_title('', false) . $after;
  
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . $userdata->display_name . $after;
  
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
  
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
  
    echo '</div>';
  
  }
}
*/
