<?php
/**
 * Responsabile initial setup and constants
 */
function oi_setup() {
	load_theme_textdomain ('officineimmaginazione', get_template_directory() . '/lang');
	register_nav_menus (array(
		'primary_navigation' => __('Menu alto', 'officineimmaginazione'),
        'secondary_navigation' => __('Menu basso', 'officineimmaginazione')
	));
	add_editor_style('style.css');
}
add_action('after_setup_theme', 'oi_setup');

/**
 * Register sidebars
 */
function oi_widgets_init() {
    register_sidebar(array(
		'name'				=> __('Social', 'officineimmaginazione'),
		'id'				=> 'site-social',
		'before_widget' 	=> '',
		'after_widget'		=> '',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
    
    
	register_sidebar(array(
		'name'				=> __('Footer - Testo sinistra', 'officineimmaginazione'),
		'id'				=> 'footer-left',
		'before_widget' 	=> '',
		'after_widget'		=> '',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
    register_sidebar(array(
		'name'				=> __('Footer - Testo centro', 'officineimmaginazione'),
		'id'				=> 'footer-center',
		'before_widget' 	=> '',
		'after_widget'		=> '',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
    register_sidebar(array(
		'name'				=> __('Footer - Testo destra', 'officineimmaginazione'),
		'id'				=> 'footer-right',
		'before_widget' 	=> '',
		'after_widget'		=> '',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
    register_sidebar(array(
		'name'				=> __('Footer - Pulsante', 'officineimmaginazione'),
		'id'				=> 'footer-button',
		'before_widget' 	=> '',
		'after_widget'		=> '',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
    
    
    
    register_sidebar(array(
		'name'				=> __('Footer - Credits', 'officineimmaginazione'),
		'id'				=> 'footer-credits',
		'before_widget' 	=> '<div class="d-flex justify-content-center align-items-center credits">',
		'after_widget'		=> '</div>',
		'before_title'		=> '<strong>',
		'after_title'		=> '</strong>',
	));
}
add_action('widgets_init', 'oi_widgets_init');
add_theme_support( 'post-thumbnails' ); 