<?php
/*
Template Name: Pagina Azienda
*/
?>

<main class="company">
    <div class="d-flex align-items-center full-img">
        <div class="d-flex align-items-center container">
            <div class="col-md-4 banner-text">
                <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pc-banner-text', true)); ?>
            </div>
            <div class="d-flex justify-content-center col-12 col-md-8 banner-img">
                <?php get_tag_picture(get_post_meta($post->ID, 'pc-banner-image', true)['id']); ?>
            </div>
            <div class="colored"></div>
        </div>
        <div class="d-flex justify-content-center align-items-center scroll">
            <div class="scroll-item">
                <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/scroll.gif" alt="scroll"/>
                <?php _e("Scroll","officineimmaginazione")?>
            </div>
        </div>
    </div>
    
    
    
    <div id="scroll-anchor" class="d-flex container first-content">
        <div class="offset-1 col-10  offset-sm-0 col-sm-5 offset-md-1 col-md-4 col-xl-3 box-left">
            <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pc-first-box-title', true)); ?>
        </div>
        <div class="offset-1 col-10 offset-sm-1 col-sm-6 col-md-5 box-right">
            <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'pc-first-box-content', true)); ?>
        </div>
    </div>
    
    
    <div class="container second-section">
        <div class="d-flex align-items-center container second-content">
            <div class="d-flex justify-content-center col-12 col-sm-12 offset-md-0 col-md-8 offset-lg-1 col-lg-5 boxed-content">
                <?php echo get_post_meta($post->ID, 'pc-second-box-title', true);?>
            </div>
            <div class="d-flex justify-content-center col-8 col-sm-12 offset-md-0 col-md-4 offset-lg-1 col-md-5 boxed-btn">
                <a href="<?php echo get_post_meta($post->ID, 'pc-second-box-button_url', true);?>" class="btn"><?php echo get_post_meta($post->ID, 'pc-second-box-button_text', true);?></a>
            </div>
            <div class="overflowed"></div>
        </div>
        <div class="container col-12 offset-md-4 col-md-8 last-content">
            <div class="last-text">
                <?php echo get_post_meta($post->ID, 'pc-last-box-content', true);?>
            </div>
        </div>
    </div>
    
    
    
    <div class="container other-models">
        <div class="col-12 offset-md-1 col-md-10 offset-lg-2 col-lg-8 other-title">
            <?php echo get_post_meta($post->ID, 'pc-others-models-title', true);?>
        </div>
        <div class="row">
            <?php for($i = 1; $i < 4; $i++){ ?>
                <div class="col-12 offset-sm-1 col-sm-10 offset-md-0 col-md-4">
                    <?php get_tag_picture(get_post_meta($post->ID,'pc-others-models-image-'.$i, true)['id'],"normal-image"); ?>
                    <?php get_tag_picture(get_post_meta($post->ID,'pc-others-models-hover-'.$i, true)['id'],"hover-image"); ?>
                    <a class="btn btn-black" href="<?php echo get_post_meta($post->ID,'pc-others-models-button-'.$i.'_url', true); ?>"><?php echo get_post_meta($post->ID,'pc-others-models-button-'.$i.'_text', true); ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
    
</main>

