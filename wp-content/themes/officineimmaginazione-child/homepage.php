<?php
/*
Template Name: Homepage
*/
?>
<main class="homepage">
  
    <div class="clip-container">
        <div class="clip">
            <div class='items'>
                <div class='item'>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
                        <rect width="100%" height="100%" fill="#FFF"/>
                        <image xmlns:xlink= "http://www.w3.org/1999/xlink" xlink:href="<?php echo get_post_meta($post->ID, 'hp-image-normal', true)['url'] ?>" width="100%" height="100%" />
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
                            <defs>
                                <clipPath id="mask">
                                    <circle id="mask-circle" cx="25%" cy="75%" r="25%" style="fill: #ffffff"/>
                                </clipPath>
                            </defs>
                            <g clip-path="url(#mask)">
                                <rect width="100%" height="100%" fill="#FFF"/>
                                <image xmlns:xlink= "http://www.w3.org/1999/xlink" xlink:href="<?php echo get_post_meta($post->ID, 'hp-image-color', true)['url'] ?>" width="100%" height="100%" />
                            </g>
                            <circle id="circle-shadow" cx="25%" cy="75%" r="25%" style="stroke: #fff; fill: transparent; stroke-width: 0;" />
                      </svg>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="d-flex container homepage-content">
        <div class="col-8 col-md-6 col-lg-6 col-xl-5 banner-content">
            <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'hp-content', true)); ?>
        </div>
        <?php 
            $positions = array("first","second","third");
            $colors = array("red","blue","yellow");
        ?>
        <div class="d-flex flex-column align-self-end col-12 offset-md-0 col-md-12 offset-lg-2 col-lg-4 offset-xl-4 col-xl-3 btn-section">
            <?php for($i = 0; $i<3; $i++){ ?>
                <div id="btn-<?php echo $i?>" class="d-flex align-items-center single-button">
                    <a href="<?php echo get_post_meta($post->ID, 'hp-button-url-'.$positions[$i], true); ?>" class="square-btn <?php echo $colors[$i]?>"></a>
                    <div class="single-content">
                        <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'hp-button-text-'.$positions[$i], true)); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</main>
