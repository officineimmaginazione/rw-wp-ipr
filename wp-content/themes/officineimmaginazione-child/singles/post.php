<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="title">
      <h1><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </div>
    <div class="description">
      <?php the_content(); ?>
    </div>
    <div class="footer">
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'officineimmaginazione'), 'after' => '</p></nav>')); ?>
    </div>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile;