<?php

//include the main class file
require_once(get_template_directory(). "/metaboxes/class/meta-box-class.php");
if (is_admin()) {
    /*
     * prefix of meta keys, optional
     * use underscore (_) at the beginning to make keys hidden, for example $prefix = '_ba_';
     *  you also can make prefix empty to disable it
     * 
     */
    $prefix = '';


    /*
      $my_meta->addText($prefix . 'text_field_id', array('name' => 'My Text ', 'gid' => 1));
      $my_meta->addWysiwyg('image_field_id', array('name' => 'My Image ', 'gid' => 2));
      $my_meta->addTextarea($prefix . 'textarea_field_id', array('name' => 'My Textarea ', 'gid' => 1));
      $my_meta->addImage($prefix . 'image_field_id', array('name' => 'My Image ', 'gid' => 1));
      $my_meta->addTextList($prefix . 'text_list_id', array('test1' => '', 'test2' => ''), array('name' => 'Pulsante ', 'gid' => 4));
      $my_meta->addSelect($prefix . 'select_field_id', array('selectkey1' => 'Select Value1', 'selectkey2' => 'Select Value2'), array('name' => 'My select ', 'std' => array('selectkey2'), 'gid' => 4));
      $my_meta->addRadio($prefix . 'radio_field_id', array('radiokey1' => 'Radio Value1', 'radiokey2' => 'Radio Value2'), array('name' => 'My Radio Filed', 'std' => array('radionkey2'), 'gid' => 4));
      $my_meta->addFile($prefix . 'file_field_id', array('name' => 'My File', 'gid' => 3));
     */
    $config = array(
        'id' => '',
        'title' => '',
        'pages' => array('post', 'page'),
        'templates' => array('homepage.php'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(),
        'local_images' => false,
        'use_with_theme' => true,
        'inGroup' => true
    );
    
    /* BOX NEWS */
    $config['id'] = 'hp-banner';
    $config['title'] = 'Banner homepage';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addImage('hp-image-normal', array('name' => 'Immagine banner base', 'gid' => 1));
    $my_meta->addImage('hp-image-color', array('name' => 'Immagine banner colorata', 'gid' => 1));
    $my_meta->addWysiwyg('hp-content', array('name' => 'Contenuto', 'gid' => 1));
    $my_meta->Finish();
    
    
    $config['id'] = 'hp-buttons';
    $config['title'] = 'Sezione pulsanti';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addWysiwyg('hp-button-text-first', array('name' => 'Contenuto', 'gid' => 2));
    $my_meta->addText('hp-button-url-first', array('name' => 'Url', 'gid' => 2));
    $my_meta->addWysiwyg('hp-button-text-second', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->addText('hp-button-url-second', array('name' => 'Url', 'gid' => 3));
    $my_meta->addWysiwyg('hp-button-text-third', array('name' => 'Contenuto', 'gid' => 4));
    $my_meta->addText('hp-button-url-third', array('name' => 'Url', 'gid' => 4));
    $my_meta->Finish();

}
