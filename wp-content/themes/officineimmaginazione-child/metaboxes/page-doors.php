<?php

//include the main class file
require_once(get_template_directory(). "/metaboxes/class/meta-box-class.php");
if (is_admin()) {
    $prefix = '';


    /*
      $my_meta->addText($prefix . 'text_field_id', array('name' => 'My Text ', 'gid' => 1));
      $my_meta->addWysiwyg('image_field_id', array('name' => 'My Image ', 'gid' => 2));
      $my_meta->addTextarea($prefix . 'textarea_field_id', array('name' => 'My Textarea ', 'gid' => 1));
      $my_meta->addImage($prefix . 'image_field_id', array('name' => 'My Image ', 'gid' => 1));
      $my_meta->addTextList($prefix . 'text_list_id', array('test1' => '', 'test2' => ''), array('name' => 'Pulsante ', 'gid' => 4));
      $my_meta->addSelect($prefix . 'select_field_id', array('selectkey1' => 'Select Value1', 'selectkey2' => 'Select Value2'), array('name' => 'My select ', 'std' => array('selectkey2'), 'gid' => 4));
      $my_meta->addRadio($prefix . 'radio_field_id', array('radiokey1' => 'Radio Value1', 'radiokey2' => 'Radio Value2'), array('name' => 'My Radio Filed', 'std' => array('radionkey2'), 'gid' => 4));
      $my_meta->addFile($prefix . 'file_field_id', array('name' => 'My File', 'gid' => 3));
      $my_meta->addColor($prefix.'color_field_id',array('name'=> 'My Color Field'));
     */
    $config = array(
        'id' => '',
        'title' => '',
        'pages' => array('post', 'page'),
        'templates' => array('page-doors.php'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(),
        'local_images' => false,
        'use_with_theme' => true,
        'inGroup' => true
    );
    
    /*BOX Immagine*/
    $config['id'] = 'pd-banner';
    $config['title'] = 'Banner';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addWysiwyg('pd-banner-text', array('name' => 'Testo Banner', 'gid' => 1));
    $my_meta->addImage('pd-banner-image', array('name' => 'Immagine Banner', 'gid' => 1));
    $my_meta->Finish();
    
    
    /*BOX Primo contenuto*/
    $config['id'] = 'pd-first-content';
    $config['title'] = 'Primo box di contenuto';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addText('pd-first-box-title', array('name' => 'Titolo', 'gid' => 1));
    $my_meta->addWysiwyg('pd-first-box-content', array('name' => 'Contenuto', 'gid' => 1));
    $my_meta->addText('pd-second-box-title', array('name' => 'Titolo', 'gid' => 2));
    $my_meta->addTextList('pd-second-box-button', array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante', 'gid' => 2));
    $my_meta->Finish();
    
    /*Informazioni Prodotto*/
    $config['id'] = 'pd-product';
    $config['title'] = 'Informazioni prodotto';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addImage('pd-product-image', array('name' => 'Immagine prodotto', 'gid' => 1));
    $my_meta->addSelect('pd-product-color', array('red' => 'Rosso', 'blue' => 'Blu', 'yellow' => 'Giallo'), array('name' => 'Seleziona il colore', 'std' => array('option1'), 'gid' => 1));
    $my_meta->addText('pd-product-title-1', array('name' => 'Titolo', 'gid' => 2));
    $my_meta->addText('pd-product-title-2', array('name' => 'Titolo', 'gid' => 2));
    $my_meta->addText('pd-product-title-3', array('name' => 'Titolo', 'gid' => 2));
    $my_meta->addWysiwyg('pd-product-content-first', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->addWysiwyg('pd-product-content-second', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->addWysiwyg('pd-product-content-third', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->Finish();
    
    
    
    /*Soluzioni prodotto*/
    $config['id'] = 'pd-solutions';
    $config['title'] = 'Soluzioni';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addText('pd-solutions-title', array('name' => 'Titolo sezione', 'gid' => 1));
    $my_meta->addWysiwyg('pd-solutions-content-first', array('name' => 'Contenuto', 'gid' => 2));
    $my_meta->addImage('pd-solutions-image-first', array('name' => 'Immagine', 'gid' => 2));
    $my_meta->addWysiwyg('pd-solutions-content-second', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->addImage('pd-solutions-image-second', array('name' => 'Immagine', 'gid' => 3));
    $my_meta->addWysiwyg('pd-solutions-content-third', array('name' => 'Contenuto', 'gid' => 4));
    $my_meta->addImage('pd-solutions-image-third', array('name' => 'Immagine', 'gid' => 4));
    $my_meta->addWysiwyg('pd-solutions-content-fourth', array('name' => 'Contenuto', 'gid' => 5));
    $my_meta->addImage('pd-solutions-image-fourth', array('name' => 'Immagine', 'gid' => 5));
    $my_meta->addWysiwyg('pd-solutions-content-fifth', array('name' => 'Contenuto', 'gid' => 6));
    $my_meta->addImage('pd-solutions-image-fifth', array('name' => 'Immagine', 'gid' => 6));
    $my_meta->Finish();
    
   
    /*Applicazioni industriali*/
    $config['id'] = 'pd-applications';
    $config['title'] = 'Applicazioni industriali';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addText('pd-applications-title', array('name' => 'Titolo', 'gid' => 1));
    $my_meta->addWysiwyg('pd-applications-left', array('name' => 'Contenuto sinistra', 'gid' => 2));
    $my_meta->addWysiwyg('pd-applications-right', array('name' => 'Contenuto destra', 'gid' => 2));
    $my_meta->Finish();
    
    
    
    /*Alcuni progetti*/
    $config['id'] = 'pd-projects';
    $config['title'] = 'Altri progetti';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addText('pd-projects-title', array('name' => 'Titolo', 'gid' => 1));
    $my_meta->addText('pd-projects-shortcode', array('name' => 'Shortcode gallery', 'gid' => 1));
    $my_meta->Finish();
    
    
    
}
