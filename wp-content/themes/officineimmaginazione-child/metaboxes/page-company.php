<?php

//include the main class file
require_once(get_template_directory(). "/metaboxes/class/meta-box-class.php");
if (is_admin()) {
    $prefix = '';


    /*
      $my_meta->addText($prefix . 'text_field_id', array('name' => 'My Text ', 'gid' => 1));
      $my_meta->addWysiwyg('image_field_id', array('name' => 'My Image ', 'gid' => 2));
      $my_meta->addTextarea($prefix . 'textarea_field_id', array('name' => 'My Textarea ', 'gid' => 1));
      $my_meta->addImage($prefix . 'image_field_id', array('name' => 'My Image ', 'gid' => 1));
      $my_meta->addTextList($prefix . 'text_list_id', array('test1' => '', 'test2' => ''), array('name' => 'Pulsante ', 'gid' => 4));
      $my_meta->addSelect($prefix . 'select_field_id', array('selectkey1' => 'Select Value1', 'selectkey2' => 'Select Value2'), array('name' => 'My select ', 'std' => array('selectkey2'), 'gid' => 4));
      $my_meta->addRadio($prefix . 'radio_field_id', array('radiokey1' => 'Radio Value1', 'radiokey2' => 'Radio Value2'), array('name' => 'My Radio Filed', 'std' => array('radionkey2'), 'gid' => 4));
      $my_meta->addFile($prefix . 'file_field_id', array('name' => 'My File', 'gid' => 3));
      $my_meta->addColor($prefix.'color_field_id',array('name'=> 'My Color Field'));
     */
    $config = array(
        'id' => '',
        'title' => '',
        'pages' => array('post', 'page'),
        'templates' => array('page-company.php'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(),
        'local_images' => false,
        'use_with_theme' => true,
        'inGroup' => true
    );
    
    /*BOX Immagine*/
    $config['id'] = 'pc-banner';
    $config['title'] = 'Banner';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addWysiwyg('pc-banner-text', array('name' => 'Testo Banner', 'gid' => 1));
    $my_meta->addImage('pc-banner-image', array('name' => 'Immagine Banner', 'gid' => 1));
    $my_meta->Finish();
    
    /*BOX Primo contenuto*/
    $config['id'] = 'pc-first-content';
    $config['title'] = 'Primo box di contenuto';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addWysiwyg('pc-first-box-title', array('name' => 'Contenuto', 'gid' => 1));
    $my_meta->addWysiwyg('pc-first-box-content', array('name' => 'Contenuto', 'gid' => 1));
    $my_meta->addWysiwyg('pc-second-box-title', array('name' => 'Contenuto', 'gid' => 2));
    $my_meta->addTextList('pc-second-box-button', array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante', 'gid' => 2));
    $my_meta->addWysiwyg('pc-last-box-content', array('name' => 'Contenuto', 'gid' => 3));
    $my_meta->Finish();
    
    
    /*Box altri modelli*/
    $config['id'] = 'pc-others-models';
    $config['title'] = 'Box altri modelli';
    $my_meta = new OI_Meta_Box($config);
    $my_meta->addText('pc-others-models-title', array('name' => 'Titolo', 'gid' => 1));
    $my_meta->addImage('pc-others-models-image-1', array('name' => 'Immagine Banner', 'gid' => 2));
    $my_meta->addImage('pc-others-models-image-2', array('name' => 'Immagine Banner', 'gid' => 2));
    $my_meta->addImage('pc-others-models-image-3', array('name' => 'Immagine Banner', 'gid' => 2));
    
    $my_meta->addImage('pc-others-models-hover-1', array('name' => 'Immagine Banner', 'gid' => 3));
    $my_meta->addImage('pc-others-models-hover-2', array('name' => 'Immagine Banner', 'gid' => 3));
    $my_meta->addImage('pc-others-models-hover-3', array('name' => 'Immagine Banner', 'gid' => 3));
    
    $my_meta->addTextList('pc-others-models-button-1', array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante', 'gid' => 4));
    $my_meta->addTextList('pc-others-models-button-2', array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante', 'gid' => 4));
    $my_meta->addTextList('pc-others-models-button-3', array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante', 'gid' => 4));
    
    $my_meta->Finish();
    
    
}
