<?php

//include the main class file
require_once(get_template_directory(). "/metaboxes/class/meta-box-class.php");
if (is_admin()) {
    $prefix = '';


    /*
      $my_meta->addText($prefix . 'text_field_id', array('name' => 'My Text ', 'gid' => 1));
      $my_meta->addWysiwyg('image_field_id', array('name' => 'My Image ', 'gid' => 2));
      $my_meta->addTextarea($prefix . 'textarea_field_id', array('name' => 'My Textarea ', 'gid' => 1));
      $my_meta->addImage($prefix . 'image_field_id', array('name' => 'My Image ', 'gid' => 1));
      $my_meta->addTextList($prefix . 'text_list_id', array('test1' => '', 'test2' => ''), array('name' => 'Pulsante ', 'gid' => 4));
      $my_meta->addSelect($prefix . 'select_field_id', array('selectkey1' => 'Select Value1', 'selectkey2' => 'Select Value2'), array('name' => 'My select ', 'std' => array('selectkey2'), 'gid' => 4));
      $my_meta->addRadio($prefix . 'radio_field_id', array('radiokey1' => 'Radio Value1', 'radiokey2' => 'Radio Value2'), array('name' => 'My Radio Filed', 'std' => array('radionkey2'), 'gid' => 4));
      $my_meta->addFile($prefix . 'file_field_id', array('name' => 'My File', 'gid' => 3));
      $my_meta->addColor($prefix.'color_field_id',array('name'=> 'My Color Field'));
     */
    $config = array(
        'id' => '',
        'title' => '',
        'pages' => array('post', 'page'),
        'templates' => array('page-press.php'),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(),
        'local_images' => false,
        'use_with_theme' => true,
        'inGroup' => true
    );
    
    
    for($i = 1; $i < 11; $i++){
        $config['id'] = 'press-btn-'.$i;
        $config['title'] = 'Pulsante '.$i;
        $my_meta = new OI_Meta_Box($config);
        $my_meta->addText('press-title-'.$i, array('name' => 'Titolo pulsante '.$i, 'gid' => $i));
        $my_meta->addText('press-subtitle-'.$i, array('name' => 'Sottotitolo pulsante '.$i, 'gid' => $i));
        $my_meta->addTextList('press-btn-'.$i, array('text' => 'Testo', 'url' => 'Url'), array('name' => 'Pulsante ', 'gid' => $i));
        $my_meta->Finish();
    }
    
    
}
