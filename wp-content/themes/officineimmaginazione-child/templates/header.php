<header class="header">
    <nav class="navbar navbar-static navbar-dark navbar-custom" role="navigation">
        <div class="col-3">
            <button class="navbar-toggler" id="navbar-side-button" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="col-6 text-center">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <img class="navbar-brand-img " src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="Logo" />
            </a>
        </div>
        <div class="col-3 language-chooser text-right">
            <ul id="language">
                <?php foreach(icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str') as $lang) : ?>
                    <li <?php echo (ICL_LANGUAGE_CODE == $lang["code"]) ? 'class="active"' : ''; ?>><a href="<?php echo $lang["url"]; ?>"><?php echo $lang["code"]; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="overlay"></div>
        <div class="navbar-side" id="navbar-side">
            <button class="navbar-closer" id="navbar-closer" type="button">
                <img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/close.png" alt="Close navbar" />
            </button>
            <?php
            if (has_nav_menu('primary_navigation')) {
                wp_nav_menu([
                    'theme_location' => 'primary_navigation',
                    'container' => false,
                    'menu_id' => '',
                    'menu_class' => 'navbar-side-menu',
                    'depth' => 2,
                    'fallback_cb' => 'oi_Nav_Walker::fallback',
                    'walker' => new oi_Nav_Walker()
                ]);
            }
            
            if (has_nav_menu('secondary_navigation')) {
                wp_nav_menu([
                    'theme_location' => 'secondary_navigation',
                    'container' => false,
                    'menu_id' => '',
                    'menu_class' => 'navbar-side-menu second-menu',
                    'depth' => 2,
                    'fallback_cb' => 'oi_Nav_Walker::fallback',
                    'walker' => new oi_Nav_Walker()
                ]);
            }
            ?>
            <?php dynamic_sidebar('site-social'); ?>
        </div>
    </nav>
</header>