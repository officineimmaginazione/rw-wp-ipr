<footer>
    <div class="footer-content">
        <div class="d-none d-lg-flex align-items-center content-desktop">
            <?php dynamic_sidebar('site-social'); ?>
            <div class="container">
                <div class="row">
                    <div class="d-flex align-items-center offset-1 col-4 col-left">
                        <?php dynamic_sidebar('footer-left'); ?>
                    </div>
                    <div class="d-flex align-items-center col-3 col-center">
                        <?php dynamic_sidebar('footer-center'); ?>
                    </div>
                    <div class="d-flex align-items-center col-2 col-right">
                        <?php dynamic_sidebar('footer-right'); ?>
                    </div>
                </div>
            </div>
            <div class="d-flex btn-box">
                <?php dynamic_sidebar('footer-button'); ?>
            </div>
        </div>
        
        <div class="d-flex d-lg-none align-items-center content-mobile">
            <div class="container">
                <div class="col-12 offset-sm-1 col-sm-10 footer-box">
                    <div class="btn-box">
                        <?php dynamic_sidebar('footer-button'); ?>
                    </div>
                    <div class="d-flex align-items-center col-12 col-left">
                        <?php dynamic_sidebar('footer-left'); ?>
                    </div>
                    <div class="d-flex align-items-center col-12 col-center">
                        <?php dynamic_sidebar('footer-center'); ?>
                    </div>
                    <div class="d-flex align-items-center col-12 col-right">
                        <?php dynamic_sidebar('footer-right'); ?>
                    </div>
                    <?php dynamic_sidebar('site-social'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php dynamic_sidebar('footer-credits'); ?>
    <span class="back-to-top"></span>
</footer>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $windowH = $(window).height();
        $headerH = $("header").height();
        $footerH = $("footer").height();
        $homeHeight = $windowH-($headerH+$footerH);
        if($("main").hasClass("homepage")){
        }
        else{
            $(".full-img").css("height",$windowH-($headerH));
        }
        $('#navbar-side-button').on('click', function () {
            $('#navbar-side').addClass('reveal');
            $('.overlay').show();
        });
        $('.overlay').on('click', function () {
            $('#navbar-side').removeClass('reveal');
            $('.overlay').hide();
        });
        $('.navbar-closer').on('click', function () {
            $('#navbar-side').removeClass('reveal');
            $('.overlay').hide();
        });
        $(".scroll-item").click(function(){
            $('html, body').animate({
                scrollTop: $("#scroll-anchor").offset().top
            }, 1000);
        });
        
        if ($('.back-to-top').length) {
			var scrollTrigger = 700, // px
				backToTop = function () {
					var scrollTop = $(window).scrollTop();
					if (scrollTop > scrollTrigger) {
						$('.back-to-top').addClass('show');
					} else {
						$('.back-to-top').removeClass('show');
					}
				};
			backToTop();
			$(window).on('scroll', function () {
				backToTop();
			});
			$('.back-to-top').on('click', function (e) {
				e.preventDefault();
				$('html,body').animate({
					scrollTop: 0
				}, 700);
			});
		}

		$(".back-to-top").click(function(){
			$("html, body").animate({scrollTop: "0"});
		});
        
    });
</script>

<?php if (is_page_template("homepage.php")) : ?>
    <script>
        jQuery(document).ready(function ($) {
            if($(window).width() > 993){
                var clipW = $(".clip-container").width();
                var homeContentW = $(".homepage-content").width();
                var bannerContentW = $(".banner-content").width();
                var btnSectionW = $(".btn-section").width();
                var padding = (clipW-homeContentW)/2;
                $(".clip-container").css("padding-left",(padding+bannerContentW/2));
                $(".clip-container").css("padding-right",(padding+btnSectionW +30));
                $windowH = $(window).height();
                $headerH = $("header").height();
                $footerH = $("footer").height();
                $homeHeight = $windowH-($headerH+$footerH);
                $(".homepage").css("height",$homeHeight);
                $(".clip-container").css("height",$(".clip-container").width());
                $(".clip-container").css("max-height",$(".homepage").height());
                $(".clip").css("margin-top",($(".homepage").height() - $(".clip-container").height())/2);
            }
            if($(window).width() > 769 && $(window).width() <= 992){
                $(".clip-container").css("padding-left",30);
                $(".clip-container").css("padding-right",30);
                $(".clip-container").css("height",$(".clip-container").width());
                $(".clip-container").css("margin-top",$(".banner-content").height());
                $(".homepage").css("height","auto");
                $(".btn-section").css("padding-top",$(".clip-container").height());
            }
            if($(window).width() > 577 && $(window).width() <= 768){
                $(".clip-container").css("height",$(".clip-container").width());
                $(".homepage").css("height","auto");
                $(".clip-container").css("margin-top",$(".banner-content").height());
                $(".btn-section").css("padding-top",$(".clip-container").height());
            }
            if($(window).width() <= 576){
                $(".homepage").css("height","auto");
                $(".clip-container").css("padding",0);
                $(".clip-container").css("padding-left",30);
                $(".clip-container").css("padding-right",30);
                $(".clip-container").css("height",$(".clip-container").width());
                $(".clip-container").css("margin-top",$(".banner-content").height());
                $(".btn-section").css("padding-top",$(".clip-container").height());
            }
            
            $("#btn-0, #btn-1, #btn-2").on("click",function(){
                if($(window).width() < 576){
                    window.location.href = $(this).find("a").attr("href");
                }
            });
            
            $(window).resize(function(){
                if($(window).width() > 993){
                    reset();
                    var clipW = $(".clip-container").width();
                    var homeContentW = $(".homepage-content").width();
                    var bannerContentW = $(".banner-content").width();
                    var btnSectionW = $(".btn-section").width();
                    var padding = (clipW-homeContentW)/2;
                    $(".clip-container").css("padding-left",(padding+bannerContentW/2));
                    $(".clip-container").css("padding-right",(padding+btnSectionW +30));
                    $windowH = $(window).height();
                    $headerH = $("header").height();
                    $footerH = $("footer").height();
                    $homeHeight = $windowH-($headerH+$footerH);
                    $(".homepage").css("height",$homeHeight);
                    $(".clip-container").css("height",$(".clip-container").width());
                    $(".clip-container").css("max-height",$(".homepage").height());
                    $(".clip").css("margin-top",($(".homepage").height() - $(".clip-container").height())/2);
                }
                if($(window).width() > 769 && $(window).width() <= 992){
                    reset();
                    $(".clip").css("margin-top",0);
                    $(".clip-container").css("padding-left",30);
                    $(".clip-container").css("padding-right",30);
                    $(".clip-container").css("height",$(".clip-container").width());
                    $(".clip-container").css("margin-top",$(".banner-content").height());
                    $(".homepage").css("height","auto");
                    $(".btn-section").css("padding-top",$(".clip-container").height());
                }
                if($(window).width() > 577 && $(window).width() <= 768){
                    $(".clip-container").css("height",$(".clip-container").width());
                    $(".homepage").css("height","auto");
                    $(".clip-container").css("margin-top",$(".banner-content").height());
                    $(".btn-section").css("padding-top",$(".clip-container").height());
                }
                if($(window).width() <= 576){
                    $(".clip").css("margin-top",0);
                    $(".homepage").css("height","auto");
                    $(".clip-container").css("padding",0);
                    $(".clip-container").css("padding-left",30);
                    $(".clip-container").css("padding-right",30);
                    $(".clip-container").css("height",$(".clip-container").width());
                    $(".clip-container").css("margin-top",$(".banner-content").height());
                    $(".btn-section").css("padding-top",$(".clip-container").height());
                }
            });
            
            function reset(){
                $(".clip-container").css("padding",0);
                $(".clip-container").css("margin",0);
            }
            var isMobile = {
                Android: function() {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                iPad: function() {
                    return navigator.userAgent.match(/iPad/i);
                },
                Opera: function() {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function() {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                any: function() {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };
            var isIpad = 0;
            if(isMobile.iPad()) {
                isIpad = 1;
            }
            
            if(isIpad == 0){
                var svgElement = document.querySelector('svg');
                var maskedElement = document.querySelector('#mask-circle');
                var circleFeedback = document.querySelector('#circle-shadow');
                var svgPoint = svgElement.createSVGPoint();
                function cursorPoint(e, svg) {
                    svgPoint.x = e.clientX;
                    svgPoint.y = e.clientY;
                    return svgPoint.matrixTransform(svg.getScreenCTM().inverse());
                }
                function update(svgCoords) {
                    maskedElement.setAttribute('cx', svgCoords.x);
                    maskedElement.setAttribute('cy', svgCoords.y);
                    circleFeedback.setAttribute('cx', svgCoords.x);
                    circleFeedback.setAttribute('cy', svgCoords.y);
                }
                window.addEventListener('mousemove', function(e) {
                  update(cursorPoint(e, svgElement));
                }, false);
                document.addEventListener('touchmove', function(e) {
                    e.preventDefault();
                    var touch = e.targetTouches[0];
                    if (touch) {
                        update(cursorPoint(touch, svgElement));
                    }
                }, false);
            }
        });
    </script>
<?php endif; ?>
    

<?php if (is_page_template("page-doors.php")) : ?>
    <script>
        jQuery(document).ready(function ($) {
            $(".banner-img img").css("max-height",$(".full-img").height());
            $(".banner-img img").css("width","auto");
            if($(window).height() < 650){
                $(".banner-img img").css("padding-top",0);
            }
            
            var id = 0;
            if($(".info-select .option").hasClass("show")){
                id = $(".info-select .show").prop('id');
                $("#c"+id).addClass("come-in");
            }
            
            if($(".tablet-select .option").hasClass("show")){
                id = $(".info-select .show").prop('id');
                $("#c"+id).addClass("come-in");
            }
            
            $('.option').on('click', function () {
                $(".info-select .option").removeClass("show");
                $(".tablet-select .option").removeClass("show");
                $(this).addClass("show");
                id = $(this).prop('id');
                $(".content").removeClass("come-in");
                $("#c"+id).addClass("come-in");
            });
			if ($(".second-content .overflowed")[0]){
				$(".second-content .overflowed").css("height",$(".second-content").height());
				$(".second-content .overflowed").css("width",($(window).width()-$(".second-content").width())/2 + $(".second-content").width());
			}
            $(".product-application .overflowed").css("height",$(".product-application").height());
            $(".product-application .overflowed").css("width",($(window).width()-$(".product-application").width())/2 + $(".product-application").width()+ 15);
            
            img = $(".product-info .image .img-fluid");
            borderH = $(".box-border").height();
            imgH = img.height();
            space = imgH-borderH;
            if($(window).width() < 992){
                tabletSelectH = $(".tablet-select").height();
                img.css("padding-top",(borderH-space)/2-tabletSelectH/2);
            }
            else{
                img.css("padding-top",(borderH-space)/2);
            }
            $(window).trigger('resize');
            $(window).resize(function(){
                img = $(".product-info .image .img-fluid");
                borderH = $(".box-border").height();
                imgH = img.height();
                space = imgH-borderH;
				if ($(".second-content .overflowed")[0]){
					$(".second-content .overflowed").css("height",$(".second-content").height());
					$(".second-content .overflowed").css("width",($(window).width()-$(".second-content").width())/2 + $(".second-content").width());
				}
                $(".product-application .overflowed").css("height",$(".product-application").height());
                $(".product-application .overflowed").css("width",($(window).width()-$(".product-application").width())/2 + $(".product-application").width()+15);
                if($(window).width() < 992){
                    tabletSelectH = $(".tablet-select").height();
                    img.css("padding-top",(borderH-space)/2-tabletSelectH/2);
                }
                else{
                    img.css("padding-top",(borderH-space)/2);
                    if($(window).height() < 650){
                        $(".banner-img img").css("padding-top",0);
                    }
                }
            });
            $(window).scroll(function(){
				if ($(".second-content .overflowed")[0]){
					$(".second-content .overflowed").css("height",$(".second-content").height());
				}
                $(".product-application .overflowed").css("height",$(".product-application").height());
            });
            
            
            $(".fg-item-inner").addClass("d-flex align-items-center");
            $(".fg-item").append("<div class='d-flex align-items-center justify-content-center hover'><i class='fas fa-search'></i><div></div></div>");
            
            
            
            
            
            $.fn.visible = function(partial) {
                var $t            = $(this),
                    $w            = $(window),
                    viewTop       = $w.scrollTop(),
                    viewBottom    = viewTop + $w.height(),
                    _top          = $t.offset().top,
                    _bottom       = _top + $t.height(),
                    compareTop    = partial === true ? _bottom : _top,
                    compareBottom = partial === true ? _top : _bottom;
                return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            };
            
            
           
            var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie <= 0 && isIE11 == false){
                $(".ie-application").css("display","none");
				if ($(".second-content .overflowed")[0]){
					if($(".second-content .overflowed").visible(true)){
						$(".second-content .overflowed").addClass("come-in");
					}
				}
                if($(".product-application .overflowed").visible(true)){
                    $(".product-application .overflowed").addClass("come-in");
                }
                $(".product-solution .single-solution").each(function(i, el) {
                    var el = $(el);
                    if (el.visible(true)) {
                        el.addClass("come-in");
                    }
                });
                $(".fg-item .fg-thumb").each(function(i, el) {
                    var el = $(el);
                    if (el.visible(true)) {
                        el.addClass("show");
                    }
                });
                $(window).scroll(function(){
					if ($(".second-content .overflowed")[0]){
						if($(".second-content .overflowed").visible(true)){
							$(".second-content .overflowed").addClass("come-in");
						}
					}
                    if($(".product-application .overflowed").visible(true)){
                        $(".product-application .overflowed").addClass("come-in");
                    }
                    $(".product-solution .single-solution").each(function(i, el) {
                        var el = $(el);
                        if (el.visible(true)) {
                            el.addClass("come-in");
                        }
                    });
                    $(".fg-item .fg-thumb").each(function(i, el) {
                        var el = $(el);
                        if (el.visible(true)) {
                            el.addClass("show");
                        }
                    });
                });
            }else{
				if ($(".second-content .overflowed")[0]){
					$(".second-content").css("background-color","#e8e8e8");
					$(".second-content").css("padding",60);
					$(".second-content .boxed-content").css("max-width","100%");
					$(".second-content .boxed-content").css("margin-left",30);
					$(".second-content .boxed-btn").css("max-width","100%");
					$(".second-content .boxed-btn").css("margin-right","30");
					$(".second-content .overflowed").css("display","none");
				}
                $(".product-application").addClass("d-none-important");
                $(".header").append("<style>.foogallery.fg-caption-hover.fg-hover-fade .fg-loaded .fg-item-inner:hover .fg-caption, .foogallery.fg-hover-fade .fg-loaded .fg-item-inner:hover .fg-thumb::before{opacity: 1 !important}</style>");
                $("").css("opacity","1");
                $(".fg-item .hover").addClass("d-none-important");
                $(".product-application").css("display","none");
                $(".boxed-content").css("display","none");
                $(window).scroll(function(){
                    $(".product-solution .single-solution").each(function(i, el) {
                        var el = $(el);
                        if (el.visible(true)) {
                            el.addClass("come-in");
                        }
                    });
                    $(".fg-item .fg-thumb").each(function(i, el) {
                        var el = $(el);
                        if (el.visible(true)) {
                            el.addClass("show");
                        }
                    });
                });
            }
        });
    </script>
<?php endif; ?>
    
<?php if (is_page_template("page-company.php")) : ?>
<script>
    jQuery(document).ready(function ($) {
        
        $(".second-content .overflowed").css("height",$(".second-content").height());
        $(".second-content .overflowed").css("width",($(window).width()-$(".second-content").width())/2 + $(".second-content").width());
        $(".product-application .overflowed").css("height",$(".product-application").height());
        
        $secondContentH = $(".second-section .second-content").height();
        if($(window).width() < 768){
            $(".second-section .last-content").css("padding-top",$secondContentH + 45 + 45);
        }else if($(window).width() < 992){
            $(".second-section .last-content").css("padding-top",$secondContentH + 60 + 45);
        }else{
            $(".second-section .last-content").css("padding-top",$secondContentH + 90 + 45);
        }
        $(".second-section .last-content").css("margin-top",($secondContentH + 45) * (-1));
        $(window).resize(function(){
            $(".second-content .overflowed").css("height",$(".second-content").height());
            $(".product-application .overflowed").css("height",$(".product-application").height());
            $secondContentH = $(".second-section .second-content").height();
            if($(window).width() < 768){
                $(".second-section .last-content").css("padding-top",$secondContentH + 45 + 45);
            }else if($(window).width() < 992){
                $(".second-section .last-content").css("padding-top",$secondContentH + 60 + 45);
            }else{
                $(".second-section .last-content").css("padding-top",$secondContentH + 90 + 45);
            }
            $(".second-section .last-content").css("margin-top",($secondContentH + 45) * (-1));
        });
        $(window).scroll(function(){
            $(".second-content .overflowed").css("height",$(".second-content").height());
            $(".product-application .overflowed").css("height",$(".product-application").height());
        });
        
        
        
        $.fn.visible = function(partial) {
            var $t            = $(this),
                $w            = $(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;
          return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };
        
        var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie <= 0 && isIE11 == false){
            
            if($(".second-section .overflowed").visible(true)){
                $(".second-section .overflowed").addClass("come-in");
            }
            if($(".second-section .boxed-content").visible(true)){
                $(".second-section .boxed-content").addClass("bordered");
            }
            $(window).scroll(function(){
                if($(".second-section .overflowed").visible(true)){
                    $(".second-section .overflowed").addClass("come-in");
                }
                if($(".second-section .boxed-content").visible(true)){
                    $(".second-section .boxed-content").addClass("bordered");
                }
            });
        }else{
            
            $(".second-content").css({"background-color": "red","z-index": 200});
            $(".boxed-btn a").css("margin-right",0);
            $(".last-content").css("z-index",-1);
            $(".boxed-content").css("color","white");
            $(".last-content").removeClass("offset-md-4");
            $(".last-content").addClass("offset-md-2");
            
            
            
            if($(window).width() >= 992 && $(window).width() < 1200){
                $(".boxed-btn a").css("padding","10px 15px");
            }else if($(window).width() >= 576 && $(window).width() < 992){
                $(".boxed-btn a").css({"padding":"10px","margin-left":"10px", "margin-right":"30px"});
                $(".second-content").removeClass("d-flex");
            }else if($(window).width() < 576){
                $(".second-section").css({"padding":"0","max-width":"none"})
                $(".second-content").css({"flex-direction":"initial"});
                $(".second-content").removeClass("d-flex");
                $(".last-content").css({"z-index":"-1","margin-left":"30px","margin-right":"30px","width":"auto"});
                $(".first-content").css({"flex-direction":"initial"});
                $(".first-content").removeClass("d-flex");
                $(".boxed-btn").removeClass("col-8");
                $(".boxed-btn").addClass("col-12");
            }
            
            $(window).resize(function(){
                if($(window).width() > 1200){
                    $(".boxed-btn a").css("padding","15px 69px");
                }else if($(window).width() >= 768 && $(window).width() < 1200){
                    $(".boxed-btn a").css({"padding":"10px 15px"});
                    $(".second-content").addClass("d-flex");
                }else if($(window).width() >= 576 && $(window).width() < 992){
                    $(".boxed-btn a").css({"padding":"10px","margin-left":"10px", "margin-right":"30px"});
                    $(".second-section").css({"max-width":"auto"});
                    $(".second-content").removeClass("d-flex");
                    $(".last-content").css({"z-index":"-1","margin-left":"","margin-right":"","width":""});
                    $(".first-content").addClass("d-flex");
                    $(".boxed-btn").removeClass("col-12");
                    $(".boxed-btn").addClass("col-8");
                    
                }else if($(window).width() < 576){
                    $(".second-section").css({"padding":"0","max-width":"none"})
                    $(".second-content").css({"flex-direction":"initial"});
                    $(".second-content").removeClass("d-flex");
                    $(".last-content").css({"z-index":"-1","margin-left":"30px","margin-right":"30px","width":"auto"});
                    $(".first-content").css({"flex-direction":"initial"});
                    $(".first-content").removeClass("d-flex");
                    $(".boxed-btn").removeClass("col-8");
                    $(".boxed-btn").addClass("col-12");
                }
            });
        }
        
        
    });
</script>
<?php endif; ?>


<?php if (is_page_template("page-contacts.php")) : ?>
    <script>
        jQuery(document).ready(function ($) {
            
            if(window.location.hash) {
                var hash = window.location.hash.substring(1);
                $('html, body').animate({
                    scrollTop: $(".second-box").offset().top-30
                }, 1000);
            } else {
                
            }
                
            if($(window).width() > 1200){
                $(".contacts-img img").css("width",$(".contacts-form").height()-30);
                $(".contacts-img").css("right",(($(".contacts-form").height()-30)/3.5)*(-1));
            }
            else if($(window).width() > 992 && $(window).width() <= 1200){
                $(".contacts-img img").css("width",$(".contacts-form").height()/1.5);
                $(".contacts-img").css("right",-$(".contacts-img img").width()/3.5);
            }
            
            
            $(window).resize(function(){
                if($(window).width() > 1200){
                    $(".contacts-img img").css("width",$(".contacts-form").height()-30);
                    $(".contacts-img").css("right",(($(".contacts-form").height()-30)/3.5)*(-1));
                }
                else if($(window).width() > 992 && $(window).width() <= 1200){
                    $(".contacts-img img").css("width",$(".contacts-form").height()/1.5);
                    $(".contacts-img").css("right",-$(".contacts-img img").width()/3.5);
                }
            });
            
            var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            if (msie > 0 && isIE11 != false){
                $(".form-container .row").css("width","100%");
            }
            
            
            
            
            
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrSmGFe2bT6F3UD_WSROgXdSebDUHUfFM&callback=initMap"></script>
<?php endif;