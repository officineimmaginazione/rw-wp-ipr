<?php
/*
Template Name: Pagina Press
*/
?>


<!--
press-title-1
press-subtitle-1
press-btn-1_text
press-btn-1_url
-->
<main class="press">
    <div class="container">
        <h1><?php echo get_the_title(); ?></h1>
        <?php for($i=1;$i<11;$i++){ ?>
            <?php if(get_post_meta($post->ID, 'press-title-'.$i, true) != ""){ ?>
                <div class="row single-document">
                    <div class="d-flex align-items-center col-12 col-sm-6 box-left">
                        <i class="far fa-file-pdf fa-2x"></i>
                        <h3><?php echo get_post_meta($post->ID, 'press-title-'.$i, true); ?></h3>
                        <?php if(get_post_meta($post->ID, 'press-subtitle-'.$i, true) != ""){
                            echo("|");
                            echo get_post_meta($post->ID, 'press-subtitle-'.$i, true);
                        }
                        ?>
                    </div>
                    <div class="d-flex align-items-center justify-content-end col-12 offset-sm-3 col-sm-3 box-right">
                        <a class="btn" href="<?php echo get_post_meta($post->ID, 'press-btn-'.$i.'_url', true); ?>"><?php echo get_post_meta($post->ID, 'press-btn-'.$i.'_text', true); ?></a>
                    </div>
                    
                    
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</main>


