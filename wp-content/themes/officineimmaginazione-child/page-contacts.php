<?php
/*
Template Name: Pagina Contatti
*/
?>

<main class="contacts-box">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-12 col-md-6 contacts-text">
                <h1><?php the_title(); ?></h1>
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'contacts-content-left', true)); ?>
                    </div>
                    <div class="col-12 col-lg-6">
                        <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'contacts-content-right', true)); ?>
                    </div>
                </div>
            </div>
            <?php $map = get_post_meta($post->ID, 'contacts-map', true); ?>
            <?php if($map){ ?>
                <div class="col-12 col-md-6 contacts-map">
                    <div id="map" style="width:100%; height:100%;"></div>
                    <?php $coords = extractGMCoords($map); ?>
                    <script>
                        var lng = <?php echo $coords[2]; ?>;
                        var lat = <?php echo $coords[1]; ?>;
                        var mrkIcon = "<?php echo get_stylesheet_directory_uri(); ?>/assets/img/map-marker.png";
                        function initMap(){
                            var map = new google.maps.Map(document.getElementById('map'),{
                                center: new google.maps.LatLng(lat, lng),
                                zoom: 13,
                                scrollwheel: false,
                                styles:[
                                    {
                                        "featureType": "water",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#d3d3d3"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "transit",
                                        "stylers": [
                                            {
                                                "color": "#808080"
                                            },
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.highway",
                                        "elementType": "geometry.stroke",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                            },
                                            {
                                                "color": "#b3b3b3"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.highway",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#ffffff"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.local",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                            },
                                            {
                                                "color": "#ffffff"
                                            },
                                            {
                                                "weight": 1.8
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.local",
                                        "elementType": "geometry.stroke",
                                        "stylers": [
                                            {
                                                "color": "#d7d7d7"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                            },
                                            {
                                                "color": "#ebebeb"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#a7a7a7"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.arterial",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#ffffff"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.arterial",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#ffffff"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "landscape",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                            },
                                            {
                                                "color": "#efefef"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#696969"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "visibility": "on"
                                            },
                                            {
                                                "color": "#737373"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi",
                                        "elementType": "labels.icon",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi",
                                        "elementType": "labels",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.arterial",
                                        "elementType": "geometry.stroke",
                                        "stylers": [
                                            {
                                                "color": "#d6d6d6"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road",
                                        "elementType": "labels.icon",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {},
                                    {
                                        "featureType": "poi",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#dadada"
                                            }
                                        ]
                                    }
                                ]
                            });
                            var marker = new google.maps.Marker({
                                position: {lat: lat, lng: lng},
                                icon: mrkIcon,
                                map: map
                            });
                        }
                    </script>
                </div>
            <?php } ?>
        </div>
    </div>
    
    <div class="second-box">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 offset-lg-0 col-lg-6 contacts-form">
                    <?php echo do_shortcode(get_post_meta($post->ID, 'contacts-form', true)); ?>
                </div>
                <div class="d-flex justify-content-center contacts-img">
                    <?php get_tag_picture(get_post_meta($post->ID, 'contacts-image', true)['id'])?>
                </div>
            </div>
        </div>
    </div>
</main>


