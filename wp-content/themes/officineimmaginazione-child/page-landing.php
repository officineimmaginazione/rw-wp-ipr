<?php
/*
Template Name: Pagina Landing
*/
?>

<main class="landing-box">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 landing-text">
                <h1><?php the_title(); ?></h1>
                <div class="row">
                    <div class="col-12">
                        <?php echo apply_filters('meta_content', get_post_meta($post->ID, 'landing-info-content', true)); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 landing-form">
                <?php echo do_shortcode(get_post_meta($post->ID, 'landing-info-form', true)); ?>
            </div>
        </div>
    </div>
</main>


